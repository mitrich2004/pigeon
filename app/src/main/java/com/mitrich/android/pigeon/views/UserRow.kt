package com.mitrich.android.pigeon.views

import android.annotation.SuppressLint
import android.content.Context
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.getDayOrTime
import com.mitrich.android.pigeon.utils.setAccentColor
import com.mitrich.android.pigeon.utils.setBothThemesColor
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.latetest_message_row.view.*
import kotlinx.android.synthetic.main.user_row.view.*
import kotlinx.android.synthetic.main.user_row.view.user_avatar_image

class UserRow(
    val user: User,
    private val defaultText: String,
    private val context: Context,
    private val yesterdayText: String
) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.user_row

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.username_text.text = user.username

        if (user.profileImageUrl != "") {
            Picasso.get().load(user.profileImageUrl).into(viewHolder.itemView.user_avatar_image)
        } else {
            Picasso.get().load(R.drawable.user).into(viewHolder.itemView.user_avatar_image)
        }

        if (user.onlineTime == "online" || user.onlineTime == "typing") {
            viewHolder.itemView.user_status_text.text = "online"
            viewHolder.itemView.user_status_text.setAccentColor(context)
        } else {
            val onlineTime = getDayOrTime(user.onlineTime.toLong(), yesterdayText, true)
            viewHolder.itemView.user_status_text.text = "$defaultText $onlineTime"
            viewHolder.itemView.user_status_text.setBothThemesColor(context)
        }
    }
}

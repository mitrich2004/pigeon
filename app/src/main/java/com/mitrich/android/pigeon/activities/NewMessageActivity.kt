package com.mitrich.android.pigeon.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.currentUser
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.*
import com.mitrich.android.pigeon.views.UserRow
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_new_message.*

class NewMessageActivity : AppCompatActivity() {
    val usersList = arrayListOf<User>()
    val adapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)

        users_recycler_view.adapter = adapter

        loading_circle.setLoadingCircleColor(this)

        if (appDarkTheme) {
            new_message_activity_toolbar.setDarkBackground(this)
            new_message_activity_layout.setDarkLayoutBackground(this)
            back_image.setImageResource(R.drawable.ic_back_dark_theme)
            search_image.setImageResource(R.drawable.ic_search_dark_theme)
        } else {
            new_message_activity_toolbar.setLightBackground(this)
            back_image.setImageResource(R.drawable.ic_back_light_theme)
            search_image.setImageResource(R.drawable.ic_search_light_theme)
        }

        back_image.setOnClickListener {
            finish()
        }

        search_image.setOnClickListener {
            new_message_activity_title.visibility = View.GONE
            new_message_edit_text.visibility = View.VISIBLE
            new_message_edit_text.requestFocus()
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)

            new_message_edit_text.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) { }

                @SuppressLint("DefaultLocale")
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    adapter.clear()
                    usersList.forEach {
                        val username = it.username
                        if (username.toLowerCase().contains(s.toString().toLowerCase())) {
                            adapter.add(
                                UserRow(
                                    it,
                                    resources.getString(R.string.was_online_text),
                                    this@NewMessageActivity,
                                    resources.getString(R.string.yesterday_text)
                                )
                            )
                        }
                    }
                }
            })

            back_image.setOnClickListener {
                adapter.clear()
                loadAllUsers()
                new_message_activity_title.visibility = View.VISIBLE
                new_message_edit_text.visibility = View.GONE
                new_message_edit_text.setText("")
                imm.hideSoftInputFromWindow(new_message_edit_text.windowToken, 0)

                back_image.setOnClickListener {
                    finish()
                }
            }
        }

        fetchUsers()
    }

    private fun fetchUsers() {
        val reference = FirebaseDatabase.getInstance().getReference("/users")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {

                adapter.setOnItemClickListener { item, _ ->
                    val userRow = item as UserRow
                    val chatLogActivityIntent =
                        Intent(this@NewMessageActivity, ChatLogActivity::class.java)
                    chatLogActivityIntent.putExtra("user", userRow.user)
                    startActivity(chatLogActivityIntent)
                    finish()
                }

                snapshot.children.forEach {
                    val user = it.getValue(User::class.java)

                    if (user != null && user.uid != currentUser!!.uid) {
                        usersList.add(user)
                    }
                }

                loadAllUsers()

                loading_circle.visibility = View.GONE
            }
        })
    }

    private fun loadAllUsers() {
        usersList.sortByDescending { it.onlineTime }
        usersList.forEach {
            adapter.add(
                UserRow(
                    it,
                    resources.getString(R.string.was_online_text),
                    this,
                    resources.getString(R.string.yesterday_text)
                )
            )
        }
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }
}

package com.mitrich.android.pigeon.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.data.ChatMessage
import com.mitrich.android.pigeon.data.LatestMessage
import com.mitrich.android.pigeon.utils.*
import com.mitrich.android.pigeon.views.LatestMessageRow
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_forward_message.*
import kotlinx.android.synthetic.main.activity_forward_message.loading_circle

class ForwardMessageActivity : AppCompatActivity() {

    private val adapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forward_message)

        forward_activity_recycler_view.adapter = adapter

        if (appDarkTheme) {
            forward_activity_toolbar.setDarkBackground(this)
            forward_activity_layout.setDarkLayoutBackground(this)
            back_image.setImageResource(R.drawable.ic_back_dark_theme)
        } else {
            forward_activity_toolbar.setLightBackground(this)
            back_image.setImageResource(R.drawable.ic_back_light_theme)
        }

        loading_circle.setLoadingCircleColor(this)
        forward_activity_recycler_view.addItemDecoration(this)

        back_image.setOnClickListener {
            finish()
        }

        adapter.setOnItemClickListener { item, _ ->
            val latestMessageRow = item as LatestMessageRow
            val chatPartner = latestMessageRow.chatPartner
            val currentUserUid = FirebaseAuth.getInstance().uid
            val forwardedMessage = intent.getParcelableExtra<ChatMessage>("forwardedMessage")

            if (currentUserUid != null && forwardedMessage != null && chatPartner != null) {
                val chatPartnerUid = chatPartner.uid
                val currentUserReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/$currentUserUid/$chatPartnerUid").push()
                val chatPartnerReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/$chatPartnerUid/$currentUserUid").push()

                val chatMessage = ChatMessage(
                    currentUserReference.key!!,
                    forwardedMessage.text,
                    currentUserUid,
                    chatPartnerUid,
                    System.currentTimeMillis() / 1000,
                    false,
                    null,
                    forwardedMessage.fromId
                )

                currentUserReference.setValue(chatMessage)
                chatPartnerReference.setValue(chatMessage)

                val reference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/$chatPartnerUid/$currentUserUid")
                reference.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        var newMessages = snapshot.getValue(LatestMessage::class.java)?.newMessages
                        if (newMessages == null) newMessages = 0
                        FirebaseDatabase.getInstance()
                            .getReference("/latest-messages/$currentUserUid/$chatPartnerUid")
                            .setValue(LatestMessage(chatMessage, 0))
                        FirebaseDatabase.getInstance()
                            .getReference("/latest-messages/$chatPartnerUid/$currentUserUid")
                            .setValue(LatestMessage(chatMessage, newMessages + 1))
                    }
                })

                ChatLogActivity().sendNotification(
                    chatMessage,
                    chatPartner.uid,
                    LatestMessagesActivity.currentUser!!
                )
                val chatLogActivityIntent = Intent(this, ChatLogActivity::class.java)
                chatLogActivityIntent.putExtra("user", chatPartner)
                chatLogActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                startActivity(chatLogActivityIntent)
                finish()
            }
        }

        fetchChatPartners()
    }

    private fun fetchChatPartners() {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val reference =
            FirebaseDatabase.getInstance().getReference("/latest-messages/$currentUserUid")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                val latestMessages = ArrayList<LatestMessage>()
                val latestUnreadMessages = ArrayList<LatestMessage>()
                snapshot.children.forEach {
                    latestMessages.add(it.getValue(LatestMessage::class.java)!!)
                }
                latestMessages.forEach {
                    if (it.newMessages > 0) {
                        latestUnreadMessages.add(it)
                    }
                }
                latestUnreadMessages.sortedByDescending { it.chatMessage!!.timeStamp }.forEach {
                    adapter.add(
                        LatestMessageRow(
                            it,
                            this@ForwardMessageActivity,
                            resources.getString(R.string.yesterday_text)
                        )
                    )
                }
                latestMessages.sortedByDescending { it.chatMessage!!.timeStamp }.forEach {
                    if (!latestUnreadMessages.contains(it)) {
                        adapter.add(
                            LatestMessageRow(
                                it,
                                this@ForwardMessageActivity,
                                resources.getString(R.string.yesterday_text)
                            )
                        )
                    }
                }
                loading_circle.visibility = View.GONE
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }
}

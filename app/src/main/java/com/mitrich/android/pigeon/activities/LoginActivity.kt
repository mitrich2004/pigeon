@file:Suppress("DEPRECATION")
package com.mitrich.android.pigeon.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.utils.clearNotifications
import com.mitrich.android.pigeon.utils.setBlueColor
import com.mitrich.android.pigeon.utils.setDarkLayoutBackground
import com.mitrich.android.pigeon.utils.setWhiteColor
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.email_edit_text
import kotlinx.android.synthetic.main.activity_login.password_edit_text
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener

class LoginActivity : AppCompatActivity(), KeyboardVisibilityEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (appDarkTheme) {
            email_edit_text.setBackgroundResource(R.drawable.edit_text_background_dark_theme)
            password_edit_text.setBackgroundResource(R.drawable.edit_text_background_dark_theme)
            login_button.setBackgroundResource(R.drawable.disabled_button_background_dark_theme)
            login_activity_layout.setDarkLayoutBackground(this)
        } else {
            email_edit_text.setBackgroundResource(R.drawable.edit_text_background_light_theme)
            password_edit_text.setBackgroundResource(R.drawable.edit_text_background_light_theme)
            login_button.setBackgroundResource(R.drawable.disabled_button_background_light_theme)
        }

        email_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyLoginButtonState()
            }
        })

        password_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyLoginButtonState()
            }
        })

        login_button.setOnClickListener {
            logInUser()
        }

        register_text.setOnClickListener {
            val registerActivityIntent = Intent(this, RegisterActivity::class.java)
            registerActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(registerActivityIntent)
            finish()
        }

        KeyboardVisibilityEvent.setEventListener(this, this)
    }

    private fun verifyLoginButtonState() {
        if (email_edit_text.text.toString().isNotBlank()
            && password_edit_text.text.toString().isNotBlank()
        ) {
            login_button.setWhiteColor(this)
            login_button.setBackgroundResource(R.drawable.active_button_background)
        } else {
            login_button.setBlueColor(this)
            if (appDarkTheme) {
                login_button.setBackgroundResource(R.drawable.disabled_button_background_dark_theme)
            } else {
                login_button.setBackgroundResource(R.drawable.disabled_button_background_light_theme)
            }
        }
    }

    private fun logInUser() {
        val email = email_edit_text.text.toString()
        val password = password_edit_text.text.toString()

        if (email.isNotBlank() && password.isNotBlank()) {
            val progressDialog =
                ProgressDialog.show(this, "", resources.getString(R.string.please_wait_text), true)
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnSuccessListener {
                    val mainActivityIntent = Intent(this, LatestMessagesActivity::class.java)
                    startActivity(mainActivityIntent)
                    finish()
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, it.message.toString(), Toast.LENGTH_SHORT).show()
                }
        }
    }

    override fun onVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            login_scroll_view.scrollTo(0, login_scroll_view.bottom)
        } else {
            login_scroll_view.scrollTo(0, login_scroll_view.top)
        }
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }
}

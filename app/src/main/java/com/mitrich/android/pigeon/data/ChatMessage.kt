package com.mitrich.android.pigeon.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ChatMessage(
    val id: String,
    var text: String,
    val fromId: String,
    val toId: String,
    val timeStamp: Long,
    val viewed: Boolean,
    val repliedMessage: ChatMessage?,
    val forwardedFrom: String?
) :
    Parcelable {
    constructor() : this("", "", "", "", -1, false, null, null)
}

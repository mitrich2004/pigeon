package com.mitrich.android.pigeon.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(
    var username: String,
    val onlineTime: String,
    var profileImageUrl: String,
    val uid: String,
    val mutedUsersList: ArrayList<String>,
    val blockedUsersList: ArrayList<String>
) : Parcelable {
    constructor() : this("", "", "", "", arrayListOf<String>(), arrayListOf<String>())
}

package com.mitrich.android.pigeon.utils

import android.app.NotificationManager
import android.content.Context

fun clearNotifications(context: Context) {
    val notificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()
}
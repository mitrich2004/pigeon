package com.mitrich.android.pigeon.views

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.google.firebase.database.*
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.ChatLogActivity
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.accentColorId
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.data.ChatMessage
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.encryptMessage
import com.mitrich.android.pigeon.utils.getTime
import com.mitrich.android.pigeon.utils.isInternetAvailable
import com.mitrich.android.pigeon.utils.showToast
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.message_from_row.view.*

class MessageRow(
    val chatMessage: ChatMessage,
    private val screenWidth: Int,
    val fromRow: Boolean,
    private val context: Context
) : Item<ViewHolder>() {

    override fun getLayout() = if (fromRow) R.layout.message_from_row else R.layout.message_to_row

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val whiteColorId = ContextCompat.getColor(context, android.R.color.white)
        val blackColorId = ContextCompat.getColor(context, android.R.color.black)
        viewHolder.itemView.message_text.text = encryptMessage(chatMessage.text)
        viewHolder.itemView.sending_time_text.text = getTime(chatMessage.timeStamp)

        val maxTextMessageWidth = screenWidth - 350
        viewHolder.itemView.message_text.maxWidth = maxTextMessageWidth
        viewHolder.itemView.message_text.measure(0, 0)

        if (fromRow) {
            if (chatMessage.viewed) {
                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_seen)
            } else {
                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_unseen)
            }

            when (accentColorId) {
                R.color.greenAccentColor -> viewHolder.itemView.message_layout.setBackgroundResource(
                    R.drawable.message_from_row_background_green
                )
                R.color.blueAccentColor -> viewHolder.itemView.message_layout.setBackgroundResource(
                    R.drawable.message_from_row_background_blue
                )
                R.color.purpleAccentColor -> viewHolder.itemView.message_layout.setBackgroundResource(
                    R.drawable.message_from_row_background_purple
                )
                R.color.orangeAccentColor -> viewHolder.itemView.message_layout.setBackgroundResource(
                    R.drawable.message_from_row_background_orange
                )
                R.color.seaAccentColor -> viewHolder.itemView.message_layout.setBackgroundResource(R.drawable.message_from_row_background_sea)
            }
            viewHolder.itemView.reply_message.setTextColor(whiteColorId)
            viewHolder.itemView.reply_username.setTextColor(whiteColorId)
            viewHolder.itemView.reply_border.setBackgroundColor(whiteColorId)
            viewHolder.itemView.forwarded_message_title_text.setTextColor(whiteColorId)
            viewHolder.itemView.from_text.setTextColor(whiteColorId)
            viewHolder.itemView.forwarded_from_username_text.setTextColor(whiteColorId)
        } else {
            if (appDarkTheme) {
                viewHolder.itemView.message_layout.setBackgroundResource(R.drawable.message_to_row_background_dark_theme)
                viewHolder.itemView.reply_message.setTextColor(whiteColorId)
                viewHolder.itemView.forwarded_message_title_text.setTextColor(whiteColorId)
                viewHolder.itemView.from_text.setTextColor(whiteColorId)
                viewHolder.itemView.forwarded_from_username_text.setTextColor(whiteColorId)
                viewHolder.itemView.message_text.setTextColor(whiteColorId)
            } else {
                viewHolder.itemView.message_layout.setBackgroundResource(R.drawable.message_to_row_background_light_theme)
                viewHolder.itemView.reply_message.setTextColor(blackColorId)
                viewHolder.itemView.forwarded_message_title_text.setTextColor(blackColorId)
                viewHolder.itemView.from_text.setTextColor(blackColorId)
                viewHolder.itemView.forwarded_from_username_text.setTextColor(blackColorId)
                viewHolder.itemView.message_text.setTextColor(blackColorId)
            }
            viewHolder.itemView.reply_border.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    accentColorId
                )
            )
            viewHolder.itemView.reply_username.setTextColor(
                ContextCompat.getColor(
                    context,
                    accentColorId
                )
            )
        }

        if (chatMessage.forwardedFrom != null) {
            viewHolder.itemView.forwarded_message_title_text.visibility = View.VISIBLE
            viewHolder.itemView.from_text.visibility = View.VISIBLE
            FirebaseDatabase.getInstance().getReference("/users/${chatMessage.forwardedFrom}")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)
                        if (user != null) {
                            viewHolder.itemView.forwarded_from_username_text.visibility =
                                View.VISIBLE
                            viewHolder.itemView.forwarded_from_username_text.text = user.username
                        }
                    }
                })
        } else {
            viewHolder.itemView.forwarded_message_title_text.visibility = View.GONE
            viewHolder.itemView.from_text.visibility = View.GONE
            viewHolder.itemView.forwarded_from_username_text.visibility = View.GONE
        }

        if (chatMessage.repliedMessage != null) {
            viewHolder.itemView.reply_layout.visibility = View.VISIBLE
            viewHolder.itemView.reply_message.text = encryptMessage(chatMessage.repliedMessage.text)
            viewHolder.itemView.reply_message.maxWidth = maxTextMessageWidth

            val reference = FirebaseDatabase.getInstance().getReference("/users/")
            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        val user = it.getValue(User::class.java)
                        if (user != null && user.uid == chatMessage.repliedMessage.fromId) {
                            viewHolder.itemView.reply_username.text = user.username
                        }
                    }
                }
            })
        } else {
            viewHolder.itemView.reply_layout.visibility = View.GONE
        }

        val constraintSet = ConstraintSet()
        constraintSet.clone(viewHolder.itemView.message_layout)

        if (viewHolder.itemView.message_text.measuredWidth == viewHolder.itemView.message_text.maxWidth) {
            constraintSet.connect(
                viewHolder.itemView.message_text.id,
                ConstraintSet.END,
                viewHolder.itemView.message_layout.id,
                ConstraintSet.END,
                0
            )

            if (viewHolder.itemView.message_text.layout.getLineWidth(viewHolder.itemView.message_text.lineCount - 1)
                    .toInt()
                >= viewHolder.itemView.message_text.maxWidth - 120
            ) {
                constraintSet.connect(
                    viewHolder.itemView.message_text.id,
                    ConstraintSet.BOTTOM,
                    viewHolder.itemView.sending_time_text.id,
                    ConstraintSet.TOP,
                    0
                )
            } else {
                constraintSet.connect(
                    viewHolder.itemView.message_text.id,
                    ConstraintSet.BOTTOM,
                    viewHolder.itemView.sending_time_text.id,
                    ConstraintSet.BOTTOM,
                    0
                )
            }
        } else {
            constraintSet.connect(
                viewHolder.itemView.message_text.id,
                ConstraintSet.END,
                viewHolder.itemView.sending_time_text.id,
                ConstraintSet.START,
                16
            )

            constraintSet.connect(
                viewHolder.itemView.message_text.id,
                ConstraintSet.BOTTOM,
                viewHolder.itemView.message_layout.id,
                ConstraintSet.BOTTOM,
                0
            )
        }

        constraintSet.applyTo(viewHolder.itemView.message_layout)

        val reference = FirebaseDatabase.getInstance().getReference("/messages/${chatMessage.fromId}/${chatMessage.toId}/${chatMessage.id}")

        reference.addChildEventListener(object: ChildEventListener {
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                when (snapshot.key!!) {
                    "text" -> {
                        viewHolder.itemView.message_text.text = encryptMessage(snapshot.getValue(String::class.java)!!)
                    }
                    "viewed" -> {
                        if (fromRow) {
                            if (snapshot.getValue(Boolean::class.java)!!) {
                                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_seen)
                            } else {
                                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_unseen)
                            }
                        }
                    }
                }
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
        })
    }
}

package com.mitrich.android.pigeon.utils

import android.annotation.SuppressLint
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appRussianLanguage
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

@SuppressLint("SimpleDateFormat")
fun getTime(timestamp: Long): String {
    val date = Date(timestamp * 1000L)
    val sdf = SimpleDateFormat("H:mm")
    return sdf.format(date)
}

@SuppressLint("SimpleDateFormat")
fun getDayOrTime(timestamp: Long, yesterdayText: String, userOnlineTime: Boolean): String {
    val currentLocale = if (appRussianLanguage) Locale("ru") else Locale("en")

    val hourMinuteFormat = SimpleDateFormat("H:mm")
    val dayOfWeekFormat = if (userOnlineTime) {
        SimpleDateFormat("EEEE", currentLocale)
    } else {
        SimpleDateFormat("E", currentLocale)
    }

    val dayMonthFormat = if (userOnlineTime) {
        SimpleDateFormat("dd.MM", currentLocale)
    } else {
        if (!appRussianLanguage) {
            SimpleDateFormat("MMM dd", currentLocale)
        } else {
            SimpleDateFormat("dd MMM", currentLocale)
        }
    }

    val hourFormat = SimpleDateFormat("H")

    val sendingDate = Date(timestamp * 1000L)
    val currentDate = Calendar.getInstance().time
    val sendingHour = hourFormat.format(sendingDate).toInt()
    val currentHour = hourFormat.format(currentDate).toInt()

    val sendingTimeInMillis = timestamp * 1000L
    val currentTimeInMillis = Calendar.getInstance().timeInMillis

    val numberOfDaysPassed =
        (TimeUnit.MILLISECONDS.toDays(currentTimeInMillis) - TimeUnit.MILLISECONDS.toDays(
            sendingTimeInMillis
        )).toInt()
    if (numberOfDaysPassed == 0) {
        return hourMinuteFormat.format(sendingDate)
    }
    if (numberOfDaysPassed == 1) {
        return if (sendingHour > currentHour) {
            hourMinuteFormat.format(sendingDate)
        } else {
            yesterdayText
        }
    }
    if (numberOfDaysPassed in 1..6) {
        return dayOfWeekFormat.format(sendingDate)
    }

    return dayMonthFormat.format(sendingDate)
}

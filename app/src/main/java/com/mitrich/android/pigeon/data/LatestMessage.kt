package com.mitrich.android.pigeon.data

class LatestMessage(val chatMessage: ChatMessage?, val newMessages: Int) {
    constructor() : this(null, 0)
}

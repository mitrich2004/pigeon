package com.mitrich.android.pigeon.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.FirebaseDatabase
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.currentUser
import com.mitrich.android.pigeon.utils.clearNotifications
import com.mitrich.android.pigeon.utils.setDarkBackground
import com.mitrich.android.pigeon.utils.setDarkLayoutBackground
import com.mitrich.android.pigeon.utils.setLightBackground
import kotlinx.android.synthetic.main.activity_change_username.*

class ChangeUsernameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_username)

        username_edit_text.setText(currentUser!!.username)

        if (appDarkTheme) {
            change_username_activity_toolbar.setDarkBackground(this)
            change_username_activity_layout.setDarkLayoutBackground(this)
            back_image.setImageResource(R.drawable.ic_back_dark_theme)
            save_image.setImageResource(R.drawable.ic_save_dark_theme)
        } else {
            change_username_activity_toolbar.setLightBackground(this)
            back_image.setImageResource(R.drawable.ic_back_light_theme)
            save_image.setImageResource(R.drawable.ic_save_light_theme)
        }

        back_image.setOnClickListener {
            intentSettingsActivity()
        }

        save_image.setOnClickListener {
            val username = username_edit_text.text.toString()
            if (username.isNotBlank()) {
                currentUser!!.username = username
                val reference = FirebaseDatabase.getInstance()
                    .getReference("/users/${currentUser!!.uid}/username")
                reference.setValue(username)
                intentSettingsActivity()
            }
        }
    }

    private fun intentSettingsActivity() {
        val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
        settingsActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(settingsActivityIntent)
        finish()
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }
}

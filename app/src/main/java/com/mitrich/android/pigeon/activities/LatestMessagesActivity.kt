package com.mitrich.android.pigeon.activities

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.gson.Gson
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.data.LatestMessage
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.data.UserData
import com.mitrich.android.pigeon.utils.*
import com.mitrich.android.pigeon.views.LatestMessageRow
import com.onesignal.OneSignal
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_latest_messages.*
import java.io.File
import java.util.*
import kotlin.collections.HashMap
import kotlin.system.exitProcess

@Suppress("DEPRECATION")
class LatestMessagesActivity : AppCompatActivity() {

    companion object {
        var currentUser: User? = null
        var appDarkTheme = false
        var appRussianLanguage = false
        var accentColorId = R.color.blueAccentColor
        var backgroundColorId = R.color.blueBackgroundColor
        const val userDataFileName = "userData.json"
    }

    private val adapter = GroupAdapter<ViewHolder>()
    val latestMessages = HashMap<String, LatestMessage>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (!File("$filesDir/$userDataFileName").exists()) {
            File.createTempFile("userData", ".json")
            appRussianLanguage = Locale.getDefault().displayLanguage == "русский"
            appDarkTheme = resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
            File("$filesDir/$userDataFileName").writeText(
                Gson().toJson(
                    UserData(
                        appDarkTheme,
                        appRussianLanguage,
                        accentColorId,
                        backgroundColorId
                    )
                )
            )
        } else {
            File("$filesDir/$userDataFileName").forEachLine {
                val user = Gson().fromJson(it, UserData::class.java)
                appDarkTheme = user.appDarkTheme
                appRussianLanguage = user.appRussianLanguage
                accentColorId = user.accentColorId
                backgroundColorId = user.backgroundColorId
            }
        }

        if (appDarkTheme) {
            setTheme(R.style.DarkAppTheme)
        } else {
            setTheme(R.style.LightAppTheme)
        }

        verifyUserLoggedIn()
        checkUsers()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latest_messages)

        if (isInternetAvailable(this)) {
            main_activity_toolbar_title.text = resources.getString(R.string.app_name)
        } else {
            main_activity_toolbar_title.text = resources.getString(R.string.connecting_text)
        }

        latest_messages_recycler_view.adapter = adapter

        loading_circle.setLoadingCircleColor(this)
        new_message_button.setImageResource(accentColorId)
        latest_messages_recycler_view.addItemDecoration(this)

        fetchLatestMessages()

        if (appDarkTheme) {
            main_activity_toolbar.setDarkBackground(this)
            main_activity_layout.setDarkLayoutBackground(this)
            settings_image.setImageResource(R.drawable.ic_dehaze_dark_theme)
        } else {
            main_activity_toolbar.setLightBackground(this)
            settings_image.setImageResource(R.drawable.ic_dehaze_light_theme)
        }

        if (appRussianLanguage) {
            setLocale("ru", baseContext)
        } else {
            setLocale("en", baseContext)
        }

        settings_image.setOnClickListener {
            if (isInternetAvailable(this) && currentUser != null) {
                val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
                settingsActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                startActivity(settingsActivityIntent)
                finish()
            } else {
                showToast(this, resources.getString(R.string.no_connection_text))
            }
        }

        new_message_button.setOnClickListener {
            if (isInternetAvailable(this) && currentUser != null) {
                val newMessageActivityIntent = Intent(this, NewMessageActivity::class.java)
                startActivity(newMessageActivityIntent)
            } else {
                showToast(this, resources.getString(R.string.no_connection_text))
            }
        }

        adapter.setOnItemClickListener { item, _ ->
            if (isInternetAvailable(this)) {
                val latestMessageRow = item as LatestMessageRow
                val chatPartner = latestMessageRow.chatPartner!!
                val chatLogActivityIntent =
                    Intent(this@LatestMessagesActivity, ChatLogActivity::class.java)
                chatLogActivityIntent.putExtra("user", chatPartner)
                startActivity(chatLogActivityIntent)
            } else {
                showToast(this, resources.getString(R.string.no_connection_text))
            }
        }
    }

    private fun verifyUserLoggedIn(): Boolean {
        val uid = FirebaseAuth.getInstance().uid
        return if (uid == null) {
            val registerActivityIntent = Intent(this, RegisterActivity::class.java)
            registerActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(registerActivityIntent)
            false
        } else {
            OneSignal.setSubscription(true)
            OneSignal.idsAvailable { userId, _ ->
                FirebaseDatabase.getInstance()
                    .getReference("/users/${FirebaseAuth.getInstance().uid}/notificationKey")
                    .setValue(userId)
            }
            fetchCurrentUser()
            manageUserStatus()
            true
        }
    }

    private fun fetchCurrentUser() {
        val uid = FirebaseAuth.getInstance().uid
        val reference = FirebaseDatabase.getInstance().getReference("/users/$uid")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                currentUser = snapshot.getValue(
                    User::class.java
                )
                checkForLatestMessages()
            }
        })
    }

    private fun manageUserStatus() {
        val connectionInfoReference = FirebaseDatabase.getInstance().getReference(".info/connected")

        connectionInfoReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                val uid = FirebaseAuth.getInstance().uid
                if (uid != null) {
                    val reference = FirebaseDatabase.getInstance().getReference("/users/$uid")
                        .child("onlineTime")
                    reference.setValue("online")
                    reference.onDisconnect()
                        .setValue((System.currentTimeMillis() / 1000).toString())
                }
            }
        })
    }

    private fun fetchLatestMessages() {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val reference =
            FirebaseDatabase.getInstance().getReference("/latest-messages/$currentUserUid")
        reference.addChildEventListener(object : ChildEventListener {
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val latestMessage = snapshot.getValue(LatestMessage::class.java) ?: return
                latestMessages[snapshot.key!!] = latestMessage
                changeLatestMessage(latestMessage)
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val latestMessage = snapshot.getValue(LatestMessage::class.java) ?: return
                latestMessages[snapshot.key!!] = latestMessage
                addLatestMessage(latestMessage)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val latestMessage = snapshot.getValue(LatestMessage::class.java) ?: return
                latestMessages.remove(snapshot.key!!)
                removeLatestMessage(latestMessage)

                if (latestMessages.isEmpty()) {
                    no_chats_text.visibility = View.VISIBLE
                }
            }

            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
        })
    }

    private fun addLatestMessage(latestMessage: LatestMessage) {
        val latestMessagesList = sortLatestMessages()

        adapter.add(
            latestMessagesList.indexOf(latestMessage),
            LatestMessageRow(
                latestMessage,
                this@LatestMessagesActivity,
                resources.getString(R.string.yesterday_text)
            )
        )

        if (latestMessagesList.size == 1) {
            loading_circle.visibility = View.GONE
            no_chats_text.visibility = View.GONE
            main_activity_toolbar_title.text = resources.getString(R.string.app_name)
        }
    }

    private fun changeLatestMessage(latestMessage: LatestMessage) {
        removeLatestMessage(latestMessage)

        val listOfLatestMessages = sortLatestMessages()

        adapter.add(
            listOfLatestMessages.indexOf(latestMessage),
            LatestMessageRow(
                latestMessage,
                this@LatestMessagesActivity,
                resources.getString(R.string.yesterday_text)
            )
        )
    }

    private fun removeLatestMessage(latestMessage: LatestMessage) {
        for (groupIndex in 0 until adapter.groupCount) {
            val latestMessageRow = adapter.getItem(groupIndex) as LatestMessageRow
            if (latestMessageRow.chatMessage!!.toId == latestMessage.chatMessage!!.toId && latestMessageRow.chatMessage.fromId == latestMessage.chatMessage.fromId) {
                adapter.removeGroup(groupIndex)
                break
            }
        }
    }

    private fun sortLatestMessages(): List<LatestMessage> {
        return latestMessages.values.sortedByDescending { it.chatMessage!!.timeStamp }
            .sortedByDescending { it.newMessages }
    }

    private fun checkForLatestMessages() {
        FirebaseDatabase.getInstance().getReference("/latest-messages/${currentUser!!.uid}")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.childrenCount.toInt() == 0) {
                        loading_circle.visibility = View.GONE
                        no_chats_text.visibility = View.VISIBLE
                    }
                }
            })
    }

    private fun checkUsers() {
        val reference = FirebaseDatabase.getInstance().getReference("/users/")
        reference.addChildEventListener(object: ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                if (!snapshot.hasChild("uid")) {
                    reference.child(snapshot.key!!).removeValue()
                }
            }

            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }
}

package com.mitrich.android.pigeon

import android.app.Application
import com.mitrich.android.pigeon.utils.clearNotifications
import com.onesignal.OneSignal

class ApplicationClass : Application() {
    override fun onCreate() {
        super.onCreate()
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
            .unsubscribeWhenNotificationsAreDisabled(false)
            .init()
        clearNotifications(this)
    }
}

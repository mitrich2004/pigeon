package com.mitrich.android.pigeon.views

import android.content.Context
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.data.LatestMessage
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.encryptMessage
import com.mitrich.android.pigeon.utils.getDayOrTime
import com.mitrich.android.pigeon.utils.setAccentColor
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.latetest_message_row.view.*
import kotlinx.android.synthetic.main.latetest_message_row.view.message_status_image

class LatestMessageRow(
    private val latestMessage: LatestMessage,
    private val context: Context,
    private val yesterdayText: String
) : Item<ViewHolder>() {

    var chatPartner: User? = null
    val chatMessage = latestMessage.chatMessage

    override fun getLayout() = R.layout.latetest_message_row

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (chatMessage != null) {
            viewHolder.itemView.time_text.text =
                getDayOrTime(chatMessage.timeStamp, yesterdayText, false)

            if (latestMessage.newMessages == 0) {
                viewHolder.itemView.new_messages_text.visibility = View.GONE
            } else {
                viewHolder.itemView.new_messages_text.text = latestMessage.newMessages.toString()
                viewHolder.itemView.new_messages_text.visibility = View.VISIBLE
            }

            viewHolder.itemView.message_status_image.setAccentColor(context)
            viewHolder.itemView.user_status_image.setAccentColor(context)

            if (chatMessage.viewed) {
                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_seen_colored)
            } else {
                viewHolder.itemView.message_status_image.setImageResource(R.drawable.ic_unseen_colored)
            }

            if (chatMessage.fromId == FirebaseAuth.getInstance().uid) {
                viewHolder.itemView.message_status_image.visibility = View.VISIBLE
            } else {
                viewHolder.itemView.message_status_image.visibility = View.GONE
            }

            val reference =
                if (chatMessage.fromId == FirebaseAuth.getInstance().uid) {
                    FirebaseDatabase.getInstance().getReference("/users/${chatMessage.toId}")
                } else {
                    FirebaseDatabase.getInstance().getReference("/users/${chatMessage.fromId}")
                }

            reference.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    val maxMessageTextWidth =
                        viewHolder.itemView.latest_message_layout.width - viewHolder.itemView.user_avatar_image.width - 180
                    viewHolder.itemView.user_message_text.maxWidth = maxMessageTextWidth
                    chatPartner = snapshot.getValue(User::class.java)
                    if (chatPartner != null) {
                        if (chatPartner!!.profileImageUrl != "") {
                            Picasso.get().load(chatPartner!!.profileImageUrl)
                                .into(viewHolder.itemView.user_avatar_image)
                        } else {
                            Picasso.get().load(R.drawable.user)
                                .into(viewHolder.itemView.user_avatar_image)
                        }
                        viewHolder.itemView.user_username_text.text = chatPartner!!.username
                        viewHolder.itemView.user_message_text.text =
                            encryptMessage(chatMessage.text)
                        if (chatPartner!!.onlineTime == "online") viewHolder.itemView.user_status_image.visibility =
                            View.VISIBLE
                        else viewHolder.itemView.user_status_image.visibility = View.GONE
                    }
                }
            })
        }
    }
}

package com.mitrich.android.pigeon.data

data class UserData(
    val appDarkTheme: Boolean,
    var appRussianLanguage: Boolean,
    var accentColorId: Int,
    var backgroundColorId: Int
)

package com.mitrich.android.pigeon.utils

import android.content.Context
import android.content.res.Configuration
import java.util.*

@Suppress("DEPRECATION")
fun setLocale(lang: String, baseContext: Context) {
    val locale = Locale(lang)
    Locale.setDefault(locale)
    val config = Configuration()
    config.locale = locale
    baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
}

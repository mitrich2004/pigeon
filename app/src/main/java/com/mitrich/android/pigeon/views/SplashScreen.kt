package com.mitrich.android.pigeon.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mitrich.android.pigeon.activities.LatestMessagesActivity

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, LatestMessagesActivity::class.java))
        finish()
    }
}

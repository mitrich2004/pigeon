package com.mitrich.android.pigeon.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.accentColorId
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.backgroundColorId
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.currentUser
import com.mitrich.android.pigeon.data.ChatMessage
import com.mitrich.android.pigeon.data.LatestMessage
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.*
import com.mitrich.android.pigeon.views.MessageRow
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.message_from_row.view.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.Boolean
import kotlin.Int
import kotlin.Long
import kotlin.String

class ChatLogActivity : AppCompatActivity() {

    val adapter = GroupAdapter<ViewHolder>()
    val messagesList = arrayListOf<String>()
    private var chatIsOpened = true

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_log)

        val listOfViews = listOf<View>(
            chat_log_toolbar,
            send_message_layout,
            message_edit_text,
            editing_bar_layout,
            replying_bar_layout,
            unblock_button
        )

        chat_log_recycler_view.adapter = adapter
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        linearLayoutManager.stackFromEnd = true
        chat_log_recycler_view.layoutManager = linearLayoutManager
        val chatPartner = intent.getParcelableExtra<User>("user")!!
        FirebaseDatabase.getInstance()
            .getReference("/users/${currentUser!!.uid}/currentChatPartner")
            .setValue(chatPartner.uid)

        fetchChatMessages(chatPartner)
        setLatestMessageIsViewed(chatPartner)
        fetchChatPartner(chatPartner)
        checkForMessages(chatPartner.uid)

        if (appDarkTheme) {
            listOfViews.forEach {
                it.setDarkBackground(this)
            }
            cancel_editing_image.setImageResource(R.drawable.ic_cancel_dark_theme)
            cancel_replying_image.setImageResource(R.drawable.ic_cancel_dark_theme)
            chat_partner_menu_image.setImageResource(R.drawable.ic_more_dark_theme)
            back_image.setImageResource(R.drawable.ic_back_dark_theme)
        } else {
            listOfViews.forEach {
                it.setLightBackground(this)
            }
            cancel_editing_image.setImageResource(R.drawable.ic_cancel_light_theme)
            cancel_replying_image.setImageResource(R.drawable.ic_cancel_light_theme)
            chat_partner_menu_image.setImageResource(R.drawable.ic_more_light_theme)
            back_image.setImageResource(R.drawable.ic_back_light_theme)
        }

        send_image.setAccentColor(this)
        unblock_button.setTextColor(ContextCompat.getColor(this, accentColorId))
        editing_image.setAccentColor(this)
        replying_image.setAccentColor(this)
        editing_text.setAccentColor(this)
        replying_username_text.setAccentColor(this)
        confirm_editing_button.setImageResource(accentColorId)
        chat_log_layout.setBackgroundResource(backgroundColorId)

        back_image.setOnClickListener {
            val latestMessagesActivityIntent = Intent(this, LatestMessagesActivity::class.java)
            latestMessagesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            chatIsOpened = false
            startActivity(latestMessagesActivityIntent)
            finish()
        }

        send_image.setOnClickListener {
            sendMessage(chatPartner, null)
        }

        cancel_editing_image.setOnClickListener {
            cancelEditing()
        }

        cancel_replying_image.setOnClickListener {
            cancelReplying(chatPartner)
        }

        unblock_button.setOnClickListener {
            blockChatPartner(chatPartner, false, send_message_layout, unblock_button)
        }

        if (chatPartner.blockedUsersList.contains(currentUser!!.uid)) {
            send_message_layout.visibility = View.GONE
            unblock_button.visibility = View.VISIBLE
            unblock_button.isEnabled = false
            unblock_button.text = "${chatPartner.username} ${getString(R.string.blocked_text)}"
        }

        if (currentUser!!.blockedUsersList.contains(chatPartner.uid)) {
            send_message_layout.visibility = View.GONE
            unblock_button.visibility = View.VISIBLE
        }

        chat_partner_menu_image.setOnClickListener {
            val chatPartnerMenu = PopupMenu(this, chat_partner_menu_image)
            chatPartnerMenu.inflate(R.menu.chat_partner_menu)

            val muteItem = chatPartnerMenu.menu.findItem(R.id.mute_chat_partner)
            val blockItem = chatPartnerMenu.menu.findItem(R.id.block_chat_partner)

            if (chatPartner.blockedUsersList.contains(currentUser!!.uid)) {
                chatPartnerMenu.menu.removeItem(R.id.block_chat_partner)
                chatPartnerMenu.menu.removeItem(R.id.mute_chat_partner)
            }

            if (currentUser!!.blockedUsersList.contains(chatPartner.uid)) {
                blockItem.setIcon(R.drawable.ic_unblock)
                blockItem.title = resources.getString(R.string.unblock_user_text)
            } else {
                blockItem.setIcon(R.drawable.ic_block)
                blockItem.title = resources.getString(R.string.block_user_text)
            }

            if (currentUser!!.mutedUsersList.contains(chatPartner.uid)) {
                muteItem.setIcon(R.drawable.ic_unmute)
                muteItem.title = resources.getString(R.string.unmute_notifications_text)
            } else {
                muteItem.setIcon(R.drawable.ic_mute)
                muteItem.title = resources.getString(R.string.mute_notifications_text)
            }

            chatPartnerMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.mute_chat_partner -> {
                        muteChatPartner(
                            chatPartner,
                            it.title == getString(R.string.mute_notifications_text)
                        )
                        true
                    }
                    R.id.block_chat_partner -> {
                        blockChatPartner(
                            chatPartner,
                            it.title == getString(R.string.block_user_text),
                            send_message_layout,
                            unblock_button
                        )
                        true
                    }
                    R.id.delete_chat -> {
                        deleteChat(chatPartner)
                        true
                    }
                    else -> false
                }
            }

            val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true

            val mPopup = fieldMPopup.get(chatPartnerMenu)

            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)

            chatPartnerMenu.show()
        }

        message_edit_text.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val userStatusReference = FirebaseDatabase.getInstance()
                    .getReference("/users/${currentUser!!.uid}/onlineTime")

                if (message_edit_text.text.isNotEmpty()) {
                    userStatusReference.setValue("typing")
                } else {
                    userStatusReference.setValue("online")
                }
            }

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })

        adapter.setOnItemClickListener { item, view ->
            if (isInternetAvailable(this)) {
                if (!chatPartner.blockedUsersList.contains(currentUser!!.uid)) {
                    val messageRow = item as MessageRow

                    if (messageRow.fromRow) {
                        val chatMessageMenu = PopupMenu(this, view.message_layout)
                        chatMessageMenu.setOnMenuItemClickListener { menu ->
                            when (menu.itemId) {
                                R.id.reply_message -> {
                                    replyMessage(messageRow.chatMessage, chatPartner)
                                    true
                                }
                                R.id.copy_message -> {
                                    copyMessage(messageRow.chatMessage.text)
                                    true
                                }
                                R.id.edit_message -> {
                                    editMessage(messageRow.chatMessage, chatPartner)
                                    true
                                }
                                R.id.forward_message -> {
                                    forwardMessage(messageRow.chatMessage)
                                    true
                                }
                                R.id.delete_message -> {
                                    deleteMessage(messageRow.chatMessage, chatPartner, true)
                                    true
                                }
                                else -> false
                            }
                        }
                        chatMessageMenu.inflate(R.menu.chat_message_menu)
                        val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                        fieldMPopup.isAccessible = true
                        val mPopup = fieldMPopup.get(chatMessageMenu)
                        mPopup.javaClass
                            .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                            .invoke(mPopup, true)
                        chatMessageMenu.show()
                    } else {
                        val chatMessageMenu = PopupMenu(this, view.message_layout)
                        chatMessageMenu.setOnMenuItemClickListener { menu ->
                            when (menu.itemId) {
                                R.id.reply_message -> {
                                    replyMessage(messageRow.chatMessage, chatPartner)
                                    true
                                }
                                R.id.forward_message -> {
                                    forwardMessage(messageRow.chatMessage)
                                    true
                                }
                                R.id.copy_message -> {
                                    copyMessage(messageRow.chatMessage.text)
                                    true
                                }
                                R.id.delete_message -> {
                                    deleteMessage(messageRow.chatMessage, chatPartner, false)
                                    true
                                }
                                else -> false
                            }
                        }
                        chatMessageMenu.inflate(R.menu.chat_partner_message_menu)
                        val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                        fieldMPopup.isAccessible = true
                        val mPopup = fieldMPopup.get(chatMessageMenu)
                        mPopup.javaClass
                            .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                            .invoke(mPopup, true)
                        chatMessageMenu.show()
                    }
                }
            } else {
                showToast(this, resources.getString(R.string.no_connection_text))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun loadChatPartnerData(avatar: String, username: String, status: String) {
        if (avatar != "") {
            Picasso.get().load(avatar).into(chat_partner_avatar)
        }
        if (username != "") {
            chat_partner_username.text = username
        }

        when (status) {
            "online" -> {
                chat_partner_status.text = resources.getString(R.string.online_text)
                chat_partner_status.setTextColor(ContextCompat.getColor(this, accentColorId))
            }
            "typing" -> {
                val chatPartner = intent.getParcelableExtra<User>("user")!!
                FirebaseDatabase.getInstance()
                    .getReference("/users/${chatPartner.uid}/currentChatPartner")
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {}

                        override fun onDataChange(snapshot: DataSnapshot) {
                            val currentChatPartnerChat = snapshot.getValue(String::class.java) ?: ""
                            if (currentChatPartnerChat == currentUser!!.uid) {
                                chat_partner_status.text = resources.getString(R.string.typing_text)
                                chat_partner_status.setAccentColor(this@ChatLogActivity)
                            }
                        }
                    })
            }
            else -> {
                if (status != "") {
                    val onlineTime = getDayOrTime(
                        status.toLong(),
                        resources.getString(R.string.yesterday_text),
                        true
                    )
                    val defaultText = resources.getString(R.string.was_online_text)
                    chat_partner_status.text = "$defaultText $onlineTime"
                    chat_partner_status.setBothThemesColor(this)
                }
            }
        }
    }

    private fun fetchChatPartner(chatPartner: User) {
        val reference = FirebaseDatabase.getInstance().getReference("/users/${chatPartner.uid}")
        reference.addChildEventListener(object : ChildEventListener {
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                var avatar = ""
                var username = ""
                var status = ""
                when (snapshot.key!!) {
                    "profileImageUrl" -> avatar = snapshot.getValue(String::class.java)!!
                    "username" -> username = snapshot.getValue(String::class.java)!!
                    "onlineTime" -> status = snapshot.getValue(String::class.java)!!
                }
                loadChatPartnerData(avatar, username, status)
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                var avatar = ""
                var username = ""
                var status = ""
                when (snapshot.key!!) {
                    "profileImageUrl" -> avatar = snapshot.getValue(String::class.java)!!
                    "username" -> username = snapshot.getValue(String::class.java)!!
                    "onlineTime" -> status = snapshot.getValue(String::class.java)!!
                }
                loadChatPartnerData(avatar, username, status)
            }

            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
        })
    }

    private fun sendMessage(chatPartner: User, repliedMessage: ChatMessage?) {
        val messageText = message_edit_text.text.toString()

        if (messageText.isNotBlank() && messageText.isNotEmpty()) {
            if (isInternetAvailable(this)) {
                message_edit_text.setText("")
                val currentUserUid = FirebaseAuth.getInstance().uid
                val chatPartnerUid = chatPartner.uid

                if (currentUserUid != null) {
                    val currentUserReference = FirebaseDatabase.getInstance()
                        .getReference("/messages/$currentUserUid/$chatPartnerUid").push()
                    val chatPartnerReference = FirebaseDatabase.getInstance()
                        .getReference("/messages/$chatPartnerUid/$currentUserUid").push()

                    val chatMessage = ChatMessage(
                        currentUserReference.key!!,
                        encryptMessage(messageText.trim()),
                        currentUserUid,
                        chatPartnerUid,
                        System.currentTimeMillis() / 1000,
                        false,
                        repliedMessage,
                        null
                    )

                    currentUserReference.setValue(chatMessage)
                    chatPartnerReference.setValue(chatMessage)

                    val reference = FirebaseDatabase.getInstance()
                        .getReference("/latest-messages/$chatPartnerUid/$currentUserUid")
                    reference.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {}

                        override fun onDataChange(snapshot: DataSnapshot) {
                            var newMessages =
                                snapshot.getValue(LatestMessage::class.java)?.newMessages
                            if (newMessages == null) newMessages = 0
                            FirebaseDatabase.getInstance()
                                .getReference("/latest-messages/$currentUserUid/$chatPartnerUid")
                                .setValue(LatestMessage(chatMessage, 0))
                            FirebaseDatabase.getInstance()
                                .getReference("/latest-messages/$chatPartnerUid/$currentUserUid")
                                .setValue(LatestMessage(chatMessage, newMessages + 1))
                        }
                    })

                    sendNotification(chatMessage, chatPartner.uid, currentUser!!)
                }
                chat_log_recycler_view.scrollToPosition(adapter.itemCount - 1)
            } else {
                showToast(this, resources.getString(R.string.no_connection_text))
            }
        }
    }

    private fun setLatestMessageIsViewed(chatPartner: User) {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val chatPartnerUid = chatPartner.uid
        val reference =
            FirebaseDatabase.getInstance()
                .getReference("/latest-messages/$chatPartnerUid/$currentUserUid")

        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                val chatMessage =
                    snapshot.getValue(LatestMessage::class.java)?.chatMessage ?: return
                if (chatMessage.fromId == chatPartnerUid && !chatMessage.viewed) {
                    reference.child("chatMessage").child("viewed").setValue(true)
                }

                val currentUserReference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/$currentUserUid/$chatPartnerUid/newMessages")

                currentUserReference.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.getValue(Long::class.java) != null) {
                            currentUserReference.setValue(0)
                        }
                    }
                })

            }
        })

    }

    private fun setMessagesAreViewedToTrue(chatPartner: User) {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val chatPartnerUid = chatPartner.uid
        val listOfMessagesKeys = arrayListOf<String>()
        val reference =
            FirebaseDatabase.getInstance().getReference("/messages/$chatPartnerUid/$currentUserUid")

        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val chatMessage = it.getValue(ChatMessage::class.java)
                    if (chatMessage!!.fromId == chatPartnerUid && !chatMessage.viewed) listOfMessagesKeys.add(
                        it.key!!
                    )
                }
                listOfMessagesKeys.forEach {
                    reference.child(it).child("viewed").setValue(true)
                }
            }
        })
        loading_circle.visibility = View.GONE
    }

    private fun fetchChatMessages(chatPartner: User) {
        val currentUserUid = FirebaseAuth.getInstance().uid
        val chatPartnerUid = chatPartner.uid
        val reference =
            FirebaseDatabase.getInstance().getReference("/messages/$currentUserUid/$chatPartnerUid")

        reference.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                if (!chatIsOpened) return
                val chatMessage = snapshot.getValue(ChatMessage::class.java)
                if (chatMessage != null && currentUser != null) {
                    if (chatMessage.fromId == currentUserUid) {
                        adapter.add(
                            MessageRow(
                                chatMessage,
                                chat_log_toolbar.width,
                                true,
                                this@ChatLogActivity
                            )
                        )
                    } else {
                        adapter.add(
                            MessageRow(
                                chatMessage,
                                chat_log_toolbar.width,
                                false,
                                this@ChatLogActivity
                            )
                        )
                    }
                    chat_log_recycler_view.scrollToPosition(adapter.itemCount - 1)
                    messagesList.add(snapshot.key!!)
                    no_messages_text.visibility = View.GONE
                    setLatestMessageIsViewed(chatPartner)
                    setMessagesAreViewedToTrue(chatPartner)
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                var messageIndex = 0
                messagesList.forEach {
                    if (it == snapshot.key!!) messageIndex = messagesList.indexOf(it)
                }

                if (messagesList.isNotEmpty()) {
                    messagesList.removeAt(messageIndex)
                    adapter.removeGroup(messageIndex)
                }

                checkForMessages(chatPartnerUid)
            }

            override fun onCancelled(error: DatabaseError) {}
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
        })
    }


    private fun replyMessage(chatMessage: ChatMessage, chatPartner: User) {
        cancelEditing()
        replying_bar_layout.visibility = View.VISIBLE
        replying_message_text.maxWidth = message_edit_text.width - 100
        replying_message_text.text = encryptMessage(chatMessage.text)
        message_edit_text.requestFocus()
        message_edit_text.setSelection(message_edit_text.text.length)
        chat_log_recycler_view.scrollBy(0, replying_bar_layout.height)

        if (chatMessage.fromId == currentUser!!.uid) {
            replying_username_text.text = currentUser!!.username
        } else {
            replying_username_text.text = chatPartner.username
        }

        if (!KeyboardVisibilityEvent.isKeyboardVisible(this)) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        }

        send_image.setOnClickListener {
            sendMessage(chatPartner, chatMessage)
            cancelReplying(chatPartner)
        }
    }

    private fun cancelReplying(chatPartner: User) {
        chat_log_recycler_view.scrollBy(0, -replying_bar_layout.height)
        replying_bar_layout.visibility = View.GONE
        replying_message_text.text = ""
        replying_username_text.text = ""
        send_image.setOnClickListener {
            sendMessage(chatPartner, null)
        }
    }

    @Suppress("DEPRECATION")
    private fun copyMessage(messageText: String) {
        val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.text = encryptMessage(messageText)
    }

    private fun editMessage(chatMessage: ChatMessage, chatPartner: User) {
        cancelReplying(chatPartner)
        editing_bar_layout.visibility = View.VISIBLE
        editing_message_text.maxWidth = message_edit_text.width - 100
        editing_message_text.text = encryptMessage(chatMessage.text)
        message_edit_text.setText(encryptMessage(chatMessage.text))
        message_edit_text.requestFocus()
        message_edit_text.setSelection(message_edit_text.text.length)
        send_image.visibility = View.INVISIBLE
        confirm_editing_button.visibility = View.VISIBLE
        confirm_editing_image.visibility = View.VISIBLE
        chat_log_recycler_view.scrollBy(0, editing_bar_layout.height)

        if (!KeyboardVisibilityEvent.isKeyboardVisible(this)) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        }

        confirm_editing_button.setOnClickListener {
            val messageText = message_edit_text.text.toString()
            message_edit_text.setText("")

            chatMessage.text = encryptMessage(messageText)

            if (messageText.isNotEmpty() && messageText.isNotBlank()) {
                val currentUserReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/${chatMessage.fromId}/${chatMessage.toId}")
                val chatPartnerReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/${chatMessage.toId}/${chatMessage.fromId}")
                val currentUserLatestMessageReference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/${chatMessage.fromId}/${chatMessage.toId}/chatMessage/text")
                val chatPartnerLatestMessageReference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/${chatMessage.toId}/${chatMessage.fromId}/chatMessage/text")

                editMessageTextInDatabase(
                    currentUserReference,
                    chatMessage.id,
                    messageText,
                    currentUserLatestMessageReference
                )
                editMessageTextInDatabase(
                    chatPartnerReference,
                    chatMessage.id,
                    messageText,
                    chatPartnerLatestMessageReference
                )
                cancelEditing()
            }
        }
    }

    private fun editMessageTextInDatabase(
        reference: DatabaseReference,
        chatMessageId: String,
        newMessageText: String,
        latestMessageReference: DatabaseReference
    ) {
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                var messageIndex = 0
                snapshot.children.forEach {
                    messageIndex++
                    val message = it.getValue(ChatMessage::class.java)
                    if (message != null && message.id == chatMessageId) {
                        reference.child(it.key!!).child("text")
                            .setValue(encryptMessage(newMessageText))

                        if (messageIndex == snapshot.childrenCount.toInt()) {
                            latestMessageReference.setValue(encryptMessage(newMessageText))
                        }
                    }
                }
            }
        })
    }

    private fun cancelEditing() {
        chat_log_recycler_view.scrollBy(0, -editing_bar_layout.height)
        editing_bar_layout.visibility = View.GONE
        editing_message_text.text = ""
        message_edit_text.setText("")
        send_image.visibility = View.VISIBLE
        confirm_editing_button.visibility = View.GONE
        confirm_editing_image.visibility = View.GONE
    }

    private fun forwardMessage(chatMessage: ChatMessage) {
        val forwardMessageActivityIntent = Intent(this, ForwardMessageActivity::class.java)
        forwardMessageActivityIntent.putExtra("forwardedMessage", chatMessage)
        startActivity(forwardMessageActivityIntent)
    }

    private fun deleteMessage(
        chatMessage: ChatMessage,
        chatPartner: User,
        currentUserMessageIsDeleted: Boolean
    ) {
        val deleteDialog = Dialog(this)
        deleteDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.delete_message_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val cancelText = findViewById<TextView>(R.id.cancel_text)
            val deleteText = findViewById<TextView>(R.id.delete_text)
            val usernameText = findViewById<TextView>(R.id.username_text)
            val checkBox = findViewById<CheckBox>(R.id.delete_checkbox)
            val checkBoxText = findViewById<TextView>(R.id.checkbox_text)
            val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)
            val checkBoxLayout = findViewById<ConstraintLayout>(R.id.checkbox_layout)
            cancelText.setAccentColor(this@ChatLogActivity)

            if (currentUserMessageIsDeleted) {
                usernameText.text = chatPartner.username
                checkBox.buttonTintList = (ColorStateList.valueOf(
                    ContextCompat.getColor(
                        this@ChatLogActivity,
                        accentColorId
                    )
                ))
            } else {
                usernameText.visibility = View.GONE
                checkBox.visibility = View.GONE
                checkBoxText.visibility = View.GONE
            }

            if (appDarkTheme) {
                layout.setBackgroundResource(R.drawable.dialog_background_dark)
            } else {
                layout.setBackgroundResource(R.drawable.dialog_background_light)
            }

            checkBoxLayout.setOnClickListener {
                checkBox.isChecked = !checkBox.isChecked
            }

            cancelText.setOnClickListener {
                dismiss()
            }

            deleteText.setOnClickListener {
                val currentUserReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/${currentUser!!.uid}/${chatPartner.uid}")
                val chatPartnerReference = FirebaseDatabase.getInstance()
                    .getReference("/messages/${chatMessage.toId}/${currentUser!!.uid}")

                val currentUserLatestMessagesReference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/${currentUser!!.uid}/${chatPartner.uid}")
                val chatPartnerLatestMessagesReference = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/${chatPartner.uid}/${currentUser!!.uid}")

                if (checkBox.isChecked) {
                    removeMessageFromLatestMessages(
                        chatPartnerLatestMessagesReference,
                        chatMessage,
                        currentUserMessageIsDeleted
                    )
                    deleteMessageFromDatabase(chatPartnerReference, chatMessage.id)
                }

                removeMessageFromLatestMessages(
                    currentUserLatestMessagesReference,
                    chatMessage,
                    currentUserMessageIsDeleted
                )
                deleteMessageFromDatabase(currentUserReference, chatMessage.id)

                dismiss()
            }
            show()
        }
    }

    private fun deleteMessageFromDatabase(reference: DatabaseReference, chatMessageId: String) {
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val message = it.getValue(ChatMessage::class.java)
                    if (message != null && message.id == chatMessageId) {
                        reference.child(it.key!!).removeValue()
                    }
                }
            }
        })
    }

    private fun removeMessageFromLatestMessages(
        latestMessagesReference: DatabaseReference,
        chatMessage: ChatMessage,
        currentUserMessageIsDeleted: Boolean
    ) {

        val reference = if (currentUserMessageIsDeleted) {
            FirebaseDatabase.getInstance()
                .getReference("/messages/${chatMessage.fromId}/${chatMessage.toId}")
        } else {
            FirebaseDatabase.getInstance()
                .getReference("/messages/${chatMessage.toId}/${chatMessage.fromId}")
        }

        if (messagesList.size == 1) {
            latestMessagesReference.removeValue()
        }

        if (messagesList.size > 1) {
            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    var messageIndex = 0
                    snapshot.children.forEach {
                        val message = it.getValue(ChatMessage::class.java)
                        if (message != null && message.id == chatMessage.id) {
                            messageIndex = messagesList.indexOf(it.key!!)
                        }
                    }

                    if (messageIndex == messagesList.size - 1) {
                        snapshot.children.forEach {
                            val previousMessage = it.getValue(ChatMessage::class.java)
                            if (previousMessage != null && it.key!! == messagesList[messagesList.size - 2]) {
                                latestMessagesReference.child("chatMessage")
                                    .setValue(previousMessage)
                            }
                        }
                    }
                }
            })
        }

        if (messagesList.size > 1 && !chatMessage.viewed) {
            latestMessagesReference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    val numberOfNewMessages =
                        snapshot.child("newMessages").getValue(Int::class.java)
                    if (numberOfNewMessages != null) {
                        if (numberOfNewMessages != 0) {
                            latestMessagesReference.child("newMessages")
                                .setValue(numberOfNewMessages - 1)
                        }
                    }
                }
            })
        }
    }

    private fun checkForMessages(chatPartnerUid: String) {
        FirebaseDatabase.getInstance()
            .getReference("/messages/${currentUser!!.uid}/$chatPartnerUid")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.childrenCount.toInt() == 0) {
                        loading_circle.visibility = View.GONE
                        no_messages_text.visibility = View.VISIBLE
                    }
                }

            })
    }

    private fun muteChatPartner(chatPartner: User, mute: Boolean) {
        if (mute) {
            currentUser!!.mutedUsersList.add(chatPartner.uid)
        } else {
            currentUser!!.mutedUsersList.remove(chatPartner.uid)
        }
        FirebaseDatabase.getInstance()
            .getReference("/users/${FirebaseAuth.getInstance().uid}/mutedUsersList")
            .setValue(currentUser!!.mutedUsersList)
    }

    @SuppressLint("SetTextI18n")
    private fun blockChatPartner(
        chatPartner: User,
        block: Boolean,
        sendMessageLayout: ConstraintLayout,
        unblockButton: Button
    ) {
        val blockDialog = Dialog(this)
        blockDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.block_user_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val cancelText = findViewById<TextView>(R.id.cancel_text)
            val blockText = findViewById<TextView>(R.id.block_text)
            val titleText = findViewById<TextView>(R.id.block_dialog_title)
            val confirmationText = findViewById<TextView>(R.id.block_message_confirmation)
            val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)

            if (block) {
                titleText.text = "${getString(R.string.block_text)} ${chatPartner.username}"
                confirmationText.text = getString(R.string.block_confirmation_text)
                blockText.text = getString(R.string.block_text)
            } else {
                titleText.text = "${getString(R.string.unblock_text)} ${chatPartner.username}"
                confirmationText.text = getString(R.string.unblock_confirmation_text)
                blockText.text = getString(R.string.okay_text)
                blockText.setAccentColor(this@ChatLogActivity)
            }

            cancelText.setAccentColor(this@ChatLogActivity)

            if (appDarkTheme) {
                layout.setBackgroundResource(R.drawable.dialog_background_dark)
            } else {
                layout.setBackgroundResource(R.drawable.dialog_background_light)
            }

            cancelText.setOnClickListener {
                dismiss()
            }

            blockText.setOnClickListener {
                if (block) {
                    currentUser!!.blockedUsersList.add(chatPartner.uid)
                    sendMessageLayout.visibility = View.GONE
                    unblockButton.visibility = View.VISIBLE
                } else {
                    currentUser!!.blockedUsersList.remove(chatPartner.uid)
                    sendMessageLayout.visibility = View.VISIBLE
                    unblockButton.visibility = View.GONE
                }
                FirebaseDatabase.getInstance()
                    .getReference("/users/${FirebaseAuth.getInstance().uid}/blockedUsersList")
                    .setValue(currentUser!!.blockedUsersList)
                dismiss()
            }

            show()
        }
    }

    private fun deleteChat(chatPartner: User) {
        val deleteDialog = Dialog(this)
        deleteDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.delete_message_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val cancelText = findViewById<TextView>(R.id.cancel_text)
            val deleteText = findViewById<TextView>(R.id.delete_text)
            val titleText = findViewById<TextView>(R.id.delete_dialog_title)
            val confirmationText = findViewById<TextView>(R.id.delete_message_confirmation)
            val usernameText = findViewById<TextView>(R.id.username_text)
            val checkBox = findViewById<CheckBox>(R.id.delete_checkbox)
            val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)
            val checkBoxLayout = findViewById<ConstraintLayout>(R.id.checkbox_layout)

            titleText.text = getString(R.string.delete_chat)
            confirmationText.text = getString(R.string.delete_chat_confirmation_text)
            usernameText.text = chatPartner.username
            deleteText.text = getString(R.string.delete_chat)
            checkBox.buttonTintList = (ColorStateList.valueOf(
                ContextCompat.getColor(
                    this@ChatLogActivity,
                    accentColorId
                )
            ))
            cancelText.setAccentColor(this@ChatLogActivity)

            if (appDarkTheme) {
                layout.setBackgroundResource(R.drawable.dialog_background_dark)
            } else {
                layout.setBackgroundResource(R.drawable.dialog_background_light)
            }

            checkBoxLayout.setOnClickListener {
                checkBox.isChecked = !checkBox.isChecked
            }

            cancelText.setOnClickListener {
                dismiss()
            }

            deleteText.setOnClickListener {
                FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/${FirebaseAuth.getInstance().uid}/${chatPartner.uid}")
                    .removeValue()
                FirebaseDatabase.getInstance()
                    .getReference("/messages/${FirebaseAuth.getInstance().uid}/${chatPartner.uid}")
                    .removeValue()

                if (checkBox.isChecked) {
                    FirebaseDatabase.getInstance()
                        .getReference("/latest-messages/${chatPartner.uid}/${FirebaseAuth.getInstance().uid}")
                        .removeValue()
                    FirebaseDatabase.getInstance()
                        .getReference("/messages/${chatPartner.uid}/${FirebaseAuth.getInstance().uid}")
                        .removeValue()
                }

                dismiss()
                finish()
            }

            show()
        }
    }

    fun sendNotification(
        chatMessage: ChatMessage,
        chatPartnerId: String,
        currentUser: User
    ) {
        FirebaseDatabase.getInstance().getReference("/users/$chatPartnerId")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    val chatPartner = snapshot.getValue(User::class.java)
                    if (chatPartner != null && !chatPartner.mutedUsersList.contains(currentUser.uid)) {
                        val message = chatMessage.text
                        val uuid = UUID.fromString("e7b47203-4fd8-46bd-8a7f-4063c59661bc")
                        val ttl = 2419200

                        val notificationKey =
                            snapshot.child("notificationKey").getValue(String::class.java) ?: ""
                        if (notificationKey != "") {
                            try {
                                val notificationContent =
                                    JSONObject(
                                        "{'contents':{'en':'${encryptMessage(
                                            message
                                        )}'},'include_player_ids':['$notificationKey']," +
                                                "'headings':{'en':'${currentUser.username}'},'android_channel_id':$uuid," +
                                                "'android_group':${currentUser.uid},'large_icon':'${currentUser.profileImageUrl}','ttl':$ttl}"
                                    )
                                OneSignal.postNotification(
                                    notificationContent,
                                    null
                                )
                            } catch (e: JSONException) {

                            }
                        }
                    }
                }
            })
    }

    private fun loadMissedMessages(chatPartner: User) {
        FirebaseDatabase.getInstance().getReference("/messages/${currentUser!!.uid}/${chatPartner.uid}").addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val chatMessage = it.getValue(ChatMessage::class.java) ?: return
                    if (!messagesList.contains(chatMessage.id)) {
                        if (chatMessage.fromId == currentUser!!.uid) {
                            adapter.add(
                                MessageRow(
                                    chatMessage,
                                    chat_log_toolbar.width,
                                    true,
                                    this@ChatLogActivity
                                )
                            )
                        } else {
                            adapter.add(
                                MessageRow(
                                    chatMessage,
                                    chat_log_toolbar.width,
                                    false,
                                    this@ChatLogActivity
                                )
                            )
                        }
                        chat_log_recycler_view.scrollToPosition(adapter.itemCount - 1)
                        messagesList.add(snapshot.key!!)
                        no_messages_text.visibility = View.GONE
                    }
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        chatIsOpened = true
        val chatPartner = intent.getParcelableExtra<User>("user")!!
        clearNotifications(this)
        FirebaseDatabase.getInstance()
            .getReference("/users/${currentUser!!.uid}/currentChatPartner")
            .setValue(chatPartner.uid)
        loadMissedMessages(chatPartner)
        setLatestMessageIsViewed(chatPartner)
        setMessagesAreViewedToTrue(chatPartner)
    }

    override fun onStop() {
        super.onStop()
        chatIsOpened = false
        FirebaseDatabase.getInstance()
            .getReference("/users/${currentUser!!.uid}/currentChatPartner").setValue("")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val latestMessagesActivityIntent = Intent(this, LatestMessagesActivity::class.java)
        latestMessagesActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(latestMessagesActivityIntent)
        finish()
    }
}

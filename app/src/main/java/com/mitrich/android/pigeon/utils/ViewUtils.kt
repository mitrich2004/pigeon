package com.mitrich.android.pigeon.utils

import android.content.Context
import android.graphics.PorterDuff
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.accentColorId

fun View.setDarkLayoutBackground(context: Context) {
    setBackgroundColor(
        ContextCompat.getColor(
            context,
            R.color.darkThemeLayoutBackground
        )
    )
}

fun View.setLightBackground(context: Context) {
    setBackgroundColor(
        ContextCompat.getColor(
            context,
            R.color.lightThemeBackgroundColor
        )
    )
}

fun View.setDarkBackground(context: Context) {
    setBackgroundColor(
        ContextCompat.getColor(
            context,
            R.color.darkThemeBackgroundColor
        )
    )
}


@Suppress("DEPRECATION")
fun ProgressBar.setLoadingCircleColor(context: Context) {
    indeterminateDrawable.setColorFilter(
        ContextCompat.getColor(
            context,
            accentColorId
        ), PorterDuff.Mode.SRC_IN
    )
}

fun RecyclerView.addItemDecoration(context: Context) {
    addItemDecoration(
        DividerItemDecoration(
            context,
            DividerItemDecoration.VERTICAL
        )
    )
}

fun ImageView.setAccentColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, accentColorId),
        PorterDuff.Mode.SRC_IN
    )
}

fun TextView.setAccentColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, accentColorId))
}

fun TextView.setBothThemesColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, R.color.bothThemesTextColor))
}

fun TextView.setBlueColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, R.color.blueAccentColor))
}

fun TextView.setWhiteColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, android.R.color.white))
}
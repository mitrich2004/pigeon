@file:Suppress("DEPRECATION")
package com.mitrich.android.pigeon.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.data.User
import com.mitrich.android.pigeon.utils.*
import kotlinx.android.synthetic.main.activity_register.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import kotlin.system.exitProcess

class RegisterActivity : AppCompatActivity(), KeyboardVisibilityEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val listOfRegistrationFields = listOf<EditText>(username_edit_text, email_edit_text, password_edit_text, confirm_password_edit_text)

        if (appDarkTheme) {
            listOfRegistrationFields.forEach {
                it.setBackgroundResource(R.drawable.edit_text_background_dark_theme)
            }
            register_button.setBackgroundResource(R.drawable.disabled_button_background_dark_theme)
            register_activity_layout.setDarkLayoutBackground(this)
        } else {
            listOfRegistrationFields.forEach {
                it.setBackgroundResource(R.drawable.edit_text_background_light_theme)
            }
            register_button.setBackgroundResource(R.drawable.disabled_button_background_light_theme)
        }

        listOfRegistrationFields.forEach {
            it.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    checkRegisterButtonState()
                }
            })
        }

        register_button.setOnClickListener {
            registerNewUser()
        }

        login_text.setOnClickListener {
            val loginActivityIntent = Intent(this, LoginActivity::class.java)
            startActivity(loginActivityIntent)
        }

        KeyboardVisibilityEvent.setEventListener(this, this)
    }

    fun checkRegisterButtonState() {
        if (username_edit_text.text.toString().isNotBlank()
            && email_edit_text.text.toString().isNotBlank()
            && password_edit_text.text.toString().isNotBlank()
            && confirm_password_edit_text.text.toString().isNotBlank()
        ) {
            register_button.setWhiteColor(this)
            register_button.setBackgroundResource(R.drawable.active_button_background)
        } else {
            register_button.setBlueColor(this)
            if (appDarkTheme) {
                register_button.setBackgroundResource(R.drawable.disabled_button_background_dark_theme)
            } else {
                register_button.setBackgroundResource(R.drawable.disabled_button_background_light_theme)
            }
        }
    }

    private fun registerNewUser() {
        val username = username_edit_text.text.toString().trim()
        val email = email_edit_text.text.toString()
        val password = password_edit_text.text.toString()
        val confirmedPassword = confirm_password_edit_text.text.toString()

        if (username.isNotBlank() && email.isNotBlank() && password.isNotBlank() && confirmedPassword.isNotBlank()) {
            if (password == confirmedPassword) {
                val progressDialog =
                    ProgressDialog.show(
                        this,
                        "",
                        resources.getString(R.string.please_wait_text),
                        true
                    )
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        saveUserToDatabase(username)
                    }
                    .addOnFailureListener {
                        progressDialog.dismiss()
                        showToast(this, it.message.toString())
                    }
            } else {
                showToast(this, resources.getString(R.string.passwords_dont_match_text))
            }
        }
    }

    private fun saveUserToDatabase(username: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val reference = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(username.trim(), "", "", uid, arrayListOf<String>(), arrayListOf<String>())

        reference.setValue(user)
            .addOnSuccessListener {
                val mainActivityIntent = Intent(this, LatestMessagesActivity::class.java)
                startActivity(mainActivityIntent)
            }
            .addOnFailureListener {
                showToast(this, it.message.toString())
            }
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }

    override fun onVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            register_scroll_view.scrollTo(0, register_scroll_view.bottom)
        } else {
            register_scroll_view.scrollTo(0, register_scroll_view.top)
        }
    }
}

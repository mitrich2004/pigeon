package com.mitrich.android.pigeon.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.widget.PopupMenu
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.exifinterface.media.ExifInterface
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.mitrich.android.pigeon.R
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.accentColorId
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appDarkTheme
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.appRussianLanguage
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.backgroundColorId
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.currentUser
import com.mitrich.android.pigeon.activities.LatestMessagesActivity.Companion.userDataFileName
import com.mitrich.android.pigeon.data.UserData
import com.mitrich.android.pigeon.utils.*
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_settings.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        if (appDarkTheme) setTheme(R.style.DarkAppTheme) else setTheme(R.style.LightAppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val listOfViews = listOf<View>(
            profile_activity_toolbar,
            app_theme_layout,
            app_language_layout,
            accent_color_layout
        )

        if (appDarkTheme) {
            listOfViews.forEach {
                it.setDarkBackground(this)
            }
            profile_activity_layout.setDarkLayoutBackground(this)
            more_image.setImageResource(R.drawable.ic_more_dark_theme)
            back_image.setImageResource(R.drawable.ic_back_dark_theme)
        } else {
            listOfViews.forEach {
                it.setLightBackground(this)
            }
            more_image.setImageResource(R.drawable.ic_more_light_theme)
            back_image.setImageResource(R.drawable.ic_back_light_theme)
        }

        val listOfButtons = arrayListOf<RadioButton>(
            light_theme_button,
            dark_theme_button,
            russian_language_button,
            english_language_button,
            blue_color_button,
            orange_color_button,
            purple_color_button,
            green_color_button,
            sea_color_button
        )

        val listOfColorButtons = arrayListOf<RadioButton>(
            blue_color_button,
            green_color_button,
            orange_color_button,
            purple_color_button,
            sea_color_button
        )

        online_status_text.setAccentColor(this)
        change_avatar_button.setImageResource(accentColorId)
        listOfButtons.forEach {
            it.buttonTintList =
                (ColorStateList.valueOf(ContextCompat.getColor(this, accentColorId)))
        }

        when (accentColorId) {
            R.color.blueAccentColor -> blue_color_button.isChecked = true
            R.color.greenAccentColor -> green_color_button.isChecked = true
            R.color.orangeAccentColor -> orange_color_button.isChecked = true
            R.color.purpleAccentColor -> purple_color_button.isChecked = true
            R.color.seaAccentColor -> sea_color_button.isChecked = true
        }

        back_image.setOnClickListener {
            val latestMessagesActivityIntent = Intent(this, LatestMessagesActivity::class.java)
            startActivity(latestMessagesActivityIntent)
        }

        username_text.text = currentUser!!.username

        if (currentUser!!.profileImageUrl != "") {
            Picasso.get().load(currentUser!!.profileImageUrl).into(user_avatar_image)
        } else {
            Picasso.get().load(R.drawable.user).into(user_avatar_image)
        }

        change_avatar_button.setOnClickListener {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                1
            )
        }

        more_image.setOnClickListener {
            val profileMenu = PopupMenu(this, it)
            profileMenu.setOnMenuItemClickListener { menu ->
                when (menu.itemId) {
                    R.id.change_name -> {
                        val changeUsernameActivityIntent =
                            Intent(this, ChangeUsernameActivity::class.java)
                        startActivity(changeUsernameActivityIntent)
                        finish()
                        true
                    }
                    R.id.exit_account -> {
                        exitAccount()
                        true
                    }
                    R.id.delete_avatar -> {
                        if (currentUser!!.profileImageUrl != "") {
                            val deleteDialog = Dialog(this)
                            deleteDialog.apply {
                                requestWindowFeature(Window.FEATURE_NO_TITLE)
                                setCancelable(true)
                                setContentView(R.layout.delete_avatar_dialog)
                                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                                val cancelText = findViewById<TextView>(R.id.cancel_text)
                                val deleteText = findViewById<TextView>(R.id.delete_text)

                                val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)

                                cancelText.setAccentColor(this@SettingsActivity)

                                if (appDarkTheme) {
                                    layout.setBackgroundResource(R.drawable.dialog_background_dark)
                                } else {
                                    layout.setBackgroundResource(R.drawable.dialog_background_light)
                                }

                                cancelText.setOnClickListener {
                                    dismiss()
                                }

                                deleteText.setOnClickListener {
                                    deleteAvatar()
                                    dismiss()
                                }
                                show()
                            }
                        } else {
                            showToast(this@SettingsActivity, resources.getString(R.string.no_avatar_text))
                        }
                        true
                    }
                    R.id.delete_account -> {
                        deleteAccount()
                        true
                    }
                    else -> false
                }
            }
            profileMenu.inflate(R.menu.profile_menu)
            val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(profileMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
            profileMenu.show()
        }

        if (appDarkTheme) {
            light_theme_button.isChecked = false
            dark_theme_button.isChecked = true
        } else {
            light_theme_button.isChecked = true
            dark_theme_button.isChecked = false
        }

        if (appRussianLanguage) {
            english_language_button.isChecked = false
            russian_language_button.isChecked = true
        } else {
            english_language_button.isChecked = true
            russian_language_button.isChecked = false
        }

        light_theme_button.setOnClickListener {
            if (appDarkTheme) {
                dark_theme_button.isChecked = false
                appDarkTheme = false
                applyChanges()
            }
        }

        dark_theme_button.setOnClickListener {
            if (!appDarkTheme) {
                light_theme_button.isChecked = false
                appDarkTheme = true
                applyChanges()
            }
        }

        english_language_button.setOnClickListener {
            if (appRussianLanguage) {
                russian_language_button.isChecked = false
                appRussianLanguage = false
                setLocale("en", baseContext)
                applyChanges()
            }
        }

        russian_language_button.setOnClickListener {
            if (!appRussianLanguage) {
                english_language_button.isChecked = false
                appRussianLanguage = true
                setLocale("ru", baseContext)
                applyChanges()
            }
        }

        blue_color_button.setOnClickListener {
            if (accentColorId != R.color.blueAccentColor) {
                accentColorId = R.color.blueAccentColor
                backgroundColorId = R.color.blueBackgroundColor
                setAccentColor(blue_color_button, listOfColorButtons)
                applyChanges()
            }
        }

        orange_color_button.setOnClickListener {
            if (accentColorId != R.color.orangeAccentColor) {
                accentColorId = R.color.orangeAccentColor
                backgroundColorId = R.color.orangeBackgroundColor
                setAccentColor(blue_color_button, listOfColorButtons)
                applyChanges()
            }
        }

        green_color_button.setOnClickListener {
            if (accentColorId != R.color.greenAccentColor) {
                accentColorId = R.color.greenAccentColor
                backgroundColorId = R.color.greenBackgroundColor
                setAccentColor(blue_color_button, listOfColorButtons)
                applyChanges()
            }
        }

        purple_color_button.setOnClickListener {
            if (accentColorId != R.color.purpleAccentColor) {
                accentColorId = R.color.purpleAccentColor
                backgroundColorId = R.color.purpleBackgroundColor
                setAccentColor(blue_color_button, listOfColorButtons)
                applyChanges()
            }
        }

        sea_color_button.setOnClickListener {
            if (accentColorId != R.color.seaAccentColor) {
                accentColorId = R.color.seaAccentColor
                backgroundColorId = R.color.seaBackgroundColor
                setAccentColor(blue_color_button, listOfColorButtons)
                applyChanges()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, 0)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && data != null) {
            user_avatar_image.setImageURI(data.data)
            val filename = UUID.randomUUID().toString()
            val bytesImage = prepareImageForSaving(data.data!!)

            FirebaseStorage.getInstance().getReference("/images/$filename").putBytes(bytesImage)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        FirebaseStorage.getInstance().reference.child("images")
                            .child(filename).downloadUrl.addOnSuccessListener { uri ->
                                FirebaseDatabase.getInstance()
                                    .getReference("users/${currentUser!!.uid}/profileImageUrl")
                                    .setValue(uri.toString())
                                currentUser!!.profileImageUrl = uri.toString()
                            }
                    }
                }
        }
    }

    private fun deleteAvatar() {
        val currentUserUid: String? = currentUser!!.uid
        if (currentUserUid != null) {
            FirebaseDatabase.getInstance().getReference("/users/$currentUserUid/profileImageUrl")
                .setValue("")
        }
        currentUser!!.profileImageUrl = ""
        Picasso.get().load(R.drawable.user).into(user_avatar_image)
    }

    private fun exitAccount() {
        val deleteDialog = Dialog(this)
        deleteDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.block_user_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val cancelText = findViewById<TextView>(R.id.cancel_text)
            val deleteText = findViewById<TextView>(R.id.block_text)
            val titleText = findViewById<TextView>(R.id.block_dialog_title)
            val confirmationText = findViewById<TextView>(R.id.block_message_confirmation)
            val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)

            titleText.text = getString(R.string.exit_account_text)
            confirmationText.text = getString(R.string.exit_account_confirmation)
            deleteText.text = getString(R.string.exit_text)

            cancelText.setAccentColor(this@SettingsActivity)

            if (appDarkTheme) {
                layout.setBackgroundResource(R.drawable.dialog_background_dark)
            } else {
                layout.setBackgroundResource(R.drawable.dialog_background_light)
            }

            cancelText.setOnClickListener {
                dismiss()
            }

            deleteText.setOnClickListener {
                val currentUserUid: String? = currentUser!!.uid
                if (currentUserUid != null) {
                    stopReceivingNotifications()
                    val reference = FirebaseDatabase.getInstance()
                        .getReference("/users/$currentUserUid/onlineTime")
                    reference.setValue((System.currentTimeMillis() / 1000).toString())
                    FirebaseAuth.getInstance().signOut()
                    val registerActivityIntent = Intent(this@SettingsActivity, RegisterActivity::class.java)
                    startActivity(registerActivityIntent)
                    finish()
                }
            }
            show()
        }
    }

    private fun deleteAccount() {
        val deleteDialog = Dialog(this)
        deleteDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.block_user_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val cancelText = findViewById<TextView>(R.id.cancel_text)
            val deleteText = findViewById<TextView>(R.id.block_text)
            val titleText = findViewById<TextView>(R.id.block_dialog_title)
            val confirmationText = findViewById<TextView>(R.id.block_message_confirmation)
            val layout = findViewById<ConstraintLayout>(R.id.delete_dialog_layout)

            titleText.text = getString(R.string.delete_account_text)
            confirmationText.text = getString(R.string.delete_account_confirmation)
            deleteText.text = getString(R.string.delete_text)

            cancelText.setAccentColor(this@SettingsActivity)

            if (appDarkTheme) {
                layout.setBackgroundResource(R.drawable.dialog_background_dark)
            } else {
                layout.setBackgroundResource(R.drawable.dialog_background_light)
            }

            cancelText.setOnClickListener {
                dismiss()
            }

            deleteText.setOnClickListener {
                FirebaseDatabase.getInstance().getReference("/users/${FirebaseAuth.getInstance().uid}").removeValue()
                FirebaseDatabase.getInstance().getReference("/messages/${FirebaseAuth.getInstance().uid}").removeValue()
                FirebaseDatabase.getInstance().getReference("/latest-messages/${FirebaseAuth.getInstance().uid}").removeValue()
                FirebaseDatabase.getInstance().getReference("/messages/").addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.children.forEach {
                            it.children.forEach { user ->
                                if (user.key!! == currentUser!!.uid) {
                                    FirebaseDatabase.getInstance().getReference("/messages/${it.key}/${user.key}/").removeValue()
                                }
                            }
                        }
                    }
                })
                FirebaseDatabase.getInstance().getReference("/latest-messages/").addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onCancelled(error: DatabaseError) {}

                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.children.forEach {
                            it.children.forEach { user ->
                                if (user.key == currentUser!!.uid) {
                                    FirebaseDatabase.getInstance().getReference("/latest-messages/${it.key}/${user.key}").removeValue()
                                }
                            }
                        }
                    }
                })
                FirebaseAuth.getInstance().currentUser!!.delete()
                FirebaseAuth.getInstance().signOut()
                val registerActivityIntent = Intent(this@SettingsActivity, RegisterActivity::class.java)
                startActivity(registerActivityIntent)
                finish()
            }

            show()
        }
    }

    private fun applyChanges() {
        File("$filesDir/$userDataFileName").writeText(
            Gson().toJson(
                UserData(
                    appDarkTheme,
                    appRussianLanguage,
                    accentColorId,
                    backgroundColorId
                )
            )
        )
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }

    private fun setAccentColor(
        checkedButton: RadioButton,
        listOfColorButtons: ArrayList<RadioButton>
    ) {
        listOfColorButtons.forEach {
            it.isChecked = it == checkedButton
        }
    }

    private fun stopReceivingNotifications() {
        FirebaseDatabase.getInstance().getReference("users/${currentUser!!.uid}/notificationKey")
            .setValue("")
        OneSignal.setSubscription(false)
    }

    @SuppressLint("Recycle")
    @Suppress("DEPRECATION")
    private fun prepareImageForSaving(image: Uri): ByteArray {
        var path: String? = null
        val projection = arrayOf(MediaStore.MediaColumns.DATA)
        val cr = applicationContext.contentResolver
        val metaCursor = cr.query(image, projection, null, null, null)
        metaCursor.use {
            if (it!!.moveToFirst()) {
                path = it.getString(0)
            }
        }

        val matrix = Matrix()
        var rotate = 0

        if (path != null) {
            val exif = ExifInterface(path!!)

            when (exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }
        }

        matrix.postRotate(rotate.toFloat())
        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, image)
        val rotatedBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        val baos = ByteArrayOutputStream()
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        return baos.toByteArray()
    }

    override fun onRestart() {
        super.onRestart()
        clearNotifications(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val latestMessagesActivityIntent = Intent(this, LatestMessagesActivity::class.java)
        startActivity(latestMessagesActivityIntent)
    }
}
